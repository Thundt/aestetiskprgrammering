function setup(){
  angleMode(DEGREES) //Vi sætter her programmet til at udregne vinkler i grader.
  rectMode(CENTER) //Vi sætter rectMode til CENTER her, da alle rectangler i programmet skal have deres origin i center.
  noStroke() //Vi sætter noStroke her, da jeg ikke vil have at nogen shapes, skal have outlines.
  noCursor() //Vi fjerner her musen fra at render inde i programmet
}

let clockCount1 = 0
let clockCount = 0
let rotation360 = -1
let rotation180 = -1

function draw(){
  createCanvas(windowWidth,windowHeight)
  background (0)

//Vi beder her programmet om at sætte "clock" til true, hver 360. frame.
  if(frameCount/360%1){
    clock=false
  } else {
    clock=true
  } //Her beder vi programmet om at tælle "clockCount" 1 op for hver gang "clock" er true, hvilket er hvert 360. frame.
  if (clock==true){
    clockCount++
  } //Hvert 2. "clockCount" beder vi programmet om at ændre "direction" værdien til negativ/positiv.
  if (clockCount/2%1){
    direction = 1
  } else {
    direction = -1
  }
  //Jeg skulle bruge endnu en "clock" så kopierede koden ovenover.
  if(frameCount/180%1){
    clock1=false
  } else {
    clock1=true
  }
  if (clock1==true){
    clockCount1++
  }
  if (clockCount1/2%1){
    direction1 = 1
  } else {
    direction1 = -1
  }
  //her beder vi programmet om at tælle værdien på rotation180/360 op eller ned, alt efter om direction(1) er positiv eller negativ.
  rotation360 = rotation360 + direction
  rotation180 = rotation180 + direction1

createThrobber(mouseX,mouseY,2) //vi kalder her den selv-lavede funktion fra nede under.

}

//Der bliver her lavet funktionen "createThrobber", så man nemt kunne lave flere af den.
function createThrobber(placementX,placementY,size){
  //Outer Circle
  push()
  fill(255)
  translate(placementX,placementY)
  scale(rotation180/80*size, rotation180/80*size)
  ellipse(0,0,50,50)
  pop()

  //square 1
  push() //push bruges før hver gang vi ændre på kordinatsystemets placering.
  fill(0)
  translate(placementX,placementY) //placementX/Y er i translate i stedet for rectanglens x/y kordinater, da vi flytter på kordinatsystemet for at placere throbberen.
  rotate(rotation360)
  scale(rotation180/100*size, rotation180/100*size) //*size er her kun da det lader en ændre størrelsen i funktionen.
  rect(0,0,50,50,1)
  pop() //pop bruges efter hver gang vi er færdige med at placere de shapes vi skal.
  //square 2
  push()
  fill(255)
  translate(placementX,placementY)
  rotate(rotation360*-2)
  scale(rotation180/140*size, rotation180/140*size)
  rect(0,0,50,50,1)
  pop()
  //square 3
  push()
  fill(0)
  translate(placementX,placementY)
  rotate(rotation360*4)
  scale(rotation180/190*size, rotation180/190*size)
  rect(0,0,50,50,1)
  pop()
  //square 4
  push()
  fill(255)
  translate(placementX,placementY)
  rotate(rotation360*-6)
  scale(rotation180/270*size, rotation180/270*size)
  rect(0,0,50,50,1)
  pop()
  //square 5
  push()
  fill(0)
  translate(placementX,placementY)
  rotate(rotation360*8)
  scale(rotation180/380*size, rotation180/380*size)
  rect(0,0,50,50,1)
  pop()
  //square 6
  push()
  fill(255)
  translate(placementX,placementY)
  rotate(rotation360*-10)
  scale(rotation180/580*size, rotation180/580*size)
  rect(0,0,50,50,1)
  pop()
  //square 7
  push()
  fill(0)
  translate(placementX,placementY)
  rotate(rotation360*12)
  scale(rotation180/800*size, rotation180/800*size)
  rect(0,0,50,50,1)
  pop()
  //square 8
  push()
  fill(255)
  translate(placementX,placementY)
  rotate(rotation360*-14)
  scale(rotation180/1800*size, rotation180/1800*size)
  rect(0,0,50,50,1)
  pop()
}