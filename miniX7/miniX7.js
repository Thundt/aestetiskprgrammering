let GV = 0
let YV = 0
let mic
let DA = 1

function setup(){
  textAlign(CENTER)
  mic = new p5.AudioIn()
  mic.start()
  frameRate(8)
  textFont('Franklin Gothic')
  noStroke()
}

function draw(){
  let canvas = createCanvas(windowWidth,windowHeight)
  canvas.mousePressed(userStartAudio)
  background(22,25,30)
  let YourValue = round(YV,6)
  let GoogleValue = round(GV,6)

//ORANGE TEXT
  fill(255,195,10)
  //Data
  textSize(width/100)
    //YV
    text("Data",30,200)
    text("risk",width/2 -40,200)
    //GV
    text("Data",width/2 + 40,200)
    text("risk",width -40,200)
  //Name: REVENUE SPY
  textSize(35)
  text("REVENUE SPY",135,65)
  //Your Paycheck:
  text("Your paycheck:", width/4 -20, 500)
  text("Your paycheck:", width/1.33 -120, 500)

//RED TEXT
  fill(255,75,75)
  text("Your data sold by you:",width/4, 250)
  text("Your data sold by Google:",width/1.33, 250)

//GREEN TEXT
  fill(75,255,75)
  //Your Paycheck:
  text(YourValue + "$", width/4 + 120, 500)
  text("Google Services", width/1.33 + 120, 500)
  //Numbers
  textSize(width/50)
  strokeWeight(5)
  text(YourValue + "$",width/4, 300)
  text(GoogleValue + "$",width/1.33, 300)

//WHITE TEXT + LINES
  //layout lines
    fill(42,45,50)
    rect(275, 10, 2, 80)
    rect(0,100,width,2)
    rect(width/2, 110, 2, height -10)
  //Feed audio message
    if(GV == 0){
      fill(255)
      text("Please feed your device some audible data to be sold!",width/1.75 ,65)
    }

//METERS     
  //DATA METERS
    //Data meters background
    fill(255,195,10, 40)
      //YV
        rect(25, 220,10, height -320)
      //GV
        rect(width/2 + 35, 220,10, height -320)
    //Data value-counter
    fill(255,195,10)
    rect(25,height -100,10, DA*-1)
    rect(width/2 + 35,height -100,10, DA*-1)

  //RISK METERS
    //YV risk
      if(GV == 0){
        //Risk background
        fill(75, 255, 75, 40)
        rect(width/2 - 45, 220,10, height -320)
        //Risk value-counter
        fill(75, 255, 75)
        rect(width/2 - 45, height -100,10, -2)
      } else {
         //Risk background
         fill(255, 75, 75, 40)
         rect(width/2 - 45, 220,10, height -320)
         //Risk value-counter
         fill(255, 75, 75)
         rect(width/2 - 45, height -100,10, -height + 330)
        }
    //GV risk
      //Risk background
      fill(75, 255, 75, 40)
      rect(width - 45, 220,10, height -320)
      //Risk value-counter
      fill(75, 255, 75)
      rect(width - 45, height -100,10, -2)

//Sound to value counter
  micLevel = mic.getLevel()
  if (micLevel > 0.01){
    GV += 0.00051
    YV += 0.0000000000000000014
    DA += 0.3
  }
}