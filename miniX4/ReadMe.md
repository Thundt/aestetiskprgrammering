# miniX4 - Capture All
![](https://gitlab.com/Thundt/aestetiskprgrammering/-/raw/main/miniX4/Screenshot.png)

[Klik her for at køre programmet](https://thundt.gitlab.io/aestetiskprgrammering/miniX4/index.html)

[Klik her for source koden](https://gitlab.com/Thundt/aestetiskprgrammering/-/blob/main/miniX4/miniX4.js)

## Revenue Spy

The fact that our data is sold by big tech companies such as Google or Meta, is no secret nowadays. What is often neglected in this discussion, is what we gain from them selling our data, being the services they provide. The reason for this is also the fact that the most important part of the discussion is, that we don’t know how much our data is worth, questions arise such as “are we being scammed?”, “are they ripping us off?” and “what counts as data, what is being sold?”. These questions have not been the focus in this project. The focus has been to bring light upon what our data is worth to us, without the “help” of companies such as Google and Meta. I like to think of this whole situation, as a hamster in a hamster wheel. We as consumers are the hamsters, the platforms Google and Meta provide is the hamster wheel, and the power being produced from turning these wheels is our data being sold. A fact in this analogy is that without the hamster wheel, no power is being generated, and we as hamsters get no entertainment. I have tried to illustrate this, in a slightly provocative way.

## Mit program

Mit program er meget simpelt. Det består af nogle linjer og noget tekst, og en variabel ved navn GV (kort for Google Value), som stiger når der kommer lyd i mikrofonen. Måden dette gøres på er først at lave en variable, i mit tilfælde `mic` som jeg tildeler `.AudioIn`, så programmet er klar til at måle lyden fra mikrofonen. Derefter skriver jeg `mic.start`, som gør at programmet rent faktisk indtager lyden fra mikrofonen. Til sidst laver jeg en ny variabel `micLevel` som jeg sætter til at være lydniveauet af mikrofonen med `mic.getLevel`. Jeg beder så programmet om at tælle variablen `GV` med 0.00062 op pr. frame (som er sat til 8) når end lydniveauet er højere end 0.01.

## Meningen med mit program

Min originale ide var faktisk at lave et hamster i et hamsterhjul, som jeg også kom ind på tidligere i min readme. Men grundet mangel på tid, og problemer med at indflette dataindsamling i selve programmet. Endte jeg med at lave en meget simpel tal-counter, som går op når data går ind.

Mit program sætter ud for kun at fremme en enkelt pointe i en ellers meget stor diskussion. At vi som individer ikke rigtig kan sælge vores data, uden hjælp fra en større platform. Spørgsmålet er bare hvad vi får for at sælge vores data, ved Google og Facebook er det for at bruge selve servicen gratis. Hvis man ville sælge sin data for capital ville dette umiddelbart være muligt igennem Streamlytics, som bruger hvad de kalder etisk data til blandt andet træning af AI. Vi er dog lidt på et punkt, hvor at hvis man vil undgå ens data bliver solgt, så må man lade vær med at bruge de hjemmesider, hvor ens data bliver genereret og dermed solgt.

Jeg mærker personligt ikke meget til at min data bliver solgt i hverdagen, og det er også derfor jeg ikke mener det er et ligeså stort problem som det bliver gjort til. Hvis jeg gjorde, ville det klart have en indflydelse på min holdning, men med brug af adblocker sker det ikke så tit. Jeg anser faktisk heller ikke min data som kun min, da der jo ikke ville være noget data, uden alle de services som jeg bruger.
