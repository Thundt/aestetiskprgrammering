let GV = 0
let mic

function setup(){
  textAlign(CENTER)
  mic = new p5.AudioIn()
  mic.start()
  frameRate(8)
}

function draw(){
  let canvas = createCanvas(windowWidth,windowHeight)
  canvas.mousePressed(userStartAudio)
  background(15)
  fill(0)
  textSize(15 + width/275*height/275)
  strokeWeight(5)

  stroke(0,255,0)
  text("0$",width/3,height/2.5)
  text(GV + "$",width/1.5,height/1.25)

  stroke(255,0,0)
  line(0,height,width,0)

  text("Your data sold by you alone:",width/3,height/4)
  text("Your data sold by Google:",width/1.5,height/1.50)

  if(GV == 0){
    stroke(255)
    text("Please feed your device some audible data to be sold!",width/2,height/1.05)
  }

  textSize(35)
  stroke(255,255,0)
  text("Revenue Spy",135,50)
  line(0,80,300,80)
  line(300,80,300,0)

  micLevel = mic.getLevel()
  if (micLevel > 0.01){
    GV += 0.00062
  }
}