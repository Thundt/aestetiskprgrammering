let min_star = 1000
let min_asteroid = 20
let star = []
let slowStar = []
let asteroid = []
let slowStarImg
let starImg
let asteroidImg
let player
let playerXPos
let playerYPos
let playerWidth = 64
let playerHeight = 80
let buttonHasBeenClicked = false
let playerHasDied = false
let score = 0

//Preloader alle lyde og billeder
function preload(){
    player = loadImage(`assets/Space_Ship.png`)
    slowStarImg = loadImage(`assets/Slow_Star.png`)
    starImg = loadImage(`assets/Star.png`)
    asteroidImg = loadImage(`assets/Asteroid.png`)
    soundFormats(`wav`)
    OST = loadSound(`assets/OST`)
}

function setup(){
    createCanvas(1440,810)
    background(0)
    playerXPos = width/2 - 32
    playerYPos = height - 100
    rectMode(CENTER)
    textSize(20)
    textFont("Arial Black")
    textAlign(CENTER)
}

function draw(){
    background(0)
//alle functioner
    checkStarNum()
    showStar()
    showSlowStar()
    deleteStar()
    deleteSlowStar()
    checkAsteroidNum()
    showAsteroid()
    deleteAsteroid()
    checkForCollision()
//score tracker
    textSize(20)
    text("Your Score: " + score, width/2, 25)
//tegner playeren
    image(player, playerXPos, playerYPos, playerWidth, playerHeight)

//Play button + loading screen
    if (buttonHasBeenClicked == false || playerHasDied){
        fill(0)
        rect(width/2, height/2, width, height)
        fill(255)
        text("Loading", width/2, height/2)
        if(frameCount > 550 || playerHasDied){
            fill(255)
            button = rect(width/2,height/2,256,128)
            fill(0)
            rect(width/2,height/2,248,120)
            fill(255)
            textSize(40)
            text("PLAY", width/2, height/2 + 12)
        }
    }   
//Music player
    if (buttonHasBeenClicked == true && OST.isPlaying() == false && playerHasDied == false){
            OST.loop()
    }

    if (playerHasDied){
        text("Play again?",width/2, 300)
        text("Your Score: " + score, width/2, 530)
    }

//score counter
    if(frameCount%60 == 0 && playerHasDied == false){
        score++
    }

    //Player Movement
        //LEFT & LEFT + UP
        if (keyIsDown(LEFT_ARROW)){
            if(keyIsDown(UP_ARROW) == false && keyIsDown(DOWN_ARROW) == false){
            playerXPos -= 10
            }
                else if (keyIsDown(UP_ARROW)){
                    playerXPos -= 8
                    playerYPos -= 8
                } 
        }
        //UP & UP + RIGHT
        if (keyIsDown(UP_ARROW)){
            if(keyIsDown(LEFT_ARROW) == false && keyIsDown(RIGHT_ARROW) == false){
            playerYPos -= 10
            }
                else if (keyIsDown(RIGHT_ARROW)){
                    playerXPos += 8
                    playerYPos -= 8
                }
        }
        //RIGHT & RIGHT + DOWN
        if (keyIsDown(RIGHT_ARROW)){
            if(keyIsDown(UP_ARROW) == false && keyIsDown(DOWN_ARROW) == false){
            playerXPos += 10
            }
                else if (keyIsDown(DOWN_ARROW)){
                    playerXPos += 8
                    playerYPos += 8
                }
        }
        
        //DOWN & DOWN + LEFT
        if (keyIsDown(DOWN_ARROW)){
            if(keyIsDown(LEFT_ARROW) == false && keyIsDown(RIGHT_ARROW) == false){
            playerYPos += 10
        }
                else if (keyIsDown(LEFT_ARROW)){
                    playerXPos -= 8
                    playerYPos += 8
                }
        }
}
//play button
function mouseClicked(){
    if (mouseX < 848 && mouseX > 592 && mouseY > 341 && mouseY < 469){
        buttonHasBeenClicked = true
        playerHasDied = false
        //resets player position
        playerXPos = width/2 - 32
        playerYPos = height - 100
        score = 0 //resets score
    }
}
//Star spawning
    function checkStarNum(){//keeps track of the amount of stars, and spawns new ones if needed
        if (star.length + slowStar.length < min_star){
            star.push(new Star())
            slowStar.push(new SlowStar())
        }
    }
    //draws and moves the big stars
    function showStar(){
        for (let i = 0; i < star.length; i++){
            star[i].move()
            star[i].show()
        }
    }
    //draws and moves the small stars
    function showSlowStar(){
        for (let i = 0; i <slowStar.length; i++){
            slowStar[i].move()
            slowStar[i].show()
        }
    }
    //Star Despawning. Deletes the stars when they reach the bottom of the window
    function deleteSlowStar(){
        for (let i = 0; i < slowStar.length; i++){
        if (slowStar[i].yPos > height){
            slowStar.splice(i,1)
        }
    }
    }
    function deleteStar(){
        for (let i = 0; i < star.length; i++){
            if (star[i].yPos > height){
            star.splice(i,1)
        }
    }
    }
    //Asteroid Spawning. Same as with the stars (except for the deletion)
    function checkAsteroidNum(){
        if (asteroid.length < min_asteroid){
            asteroid.push(new Asteroid())
        }
    }
    function showAsteroid(){
        for (let i = 0; i < asteroid.length; i++){
            if(buttonHasBeenClicked == true)
            asteroid[i].move()
            asteroid[i].show()
        }
    }
    function deleteAsteroid(){
        for (let i = 0; i < asteroid.length; i++){
            if (asteroid[i].yPos > height){
            asteroid.splice(i,1)
        }
        if (playerHasDied){//deletes all asteroids if the player has died
            asteroid.splice(asteroid.lenght)
        }
    }
    }

    //Asteroid collision
    function checkForCollision(){//checks the distance between asteroids and the player.
        for (let i = 0; i < asteroid.length; i++){
            let d = dist(playerXPos, playerYPos, asteroid[i].xPos, asteroid[i].yPos)
                if (d < 64) {//if the player is hit the music stops, and the buttonHasBeenClicked is set to false, so it can be clicked again.
                    OST.stop()
                    playerHasDied = true
                    buttonHasBeenClicked = false
                }
    }
}