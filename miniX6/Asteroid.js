class Asteroid {
    constructor(){//contains the needed info
        this.speed = (random(4,9))
        this.xPos = random(width)
        this.yPos = -70
    }
    move(){//moves the asteroid
        this.yPos += this.speed
    }
    show(){//draws the asteroid
        push()
        image(asteroidImg,this.xPos,this.yPos,64,64)
        pop()
    }
}