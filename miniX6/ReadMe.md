# miniX6 - Space Evader
![](https://gitlab.com/Thundt/aestetiskprgrammering/-/raw/main/miniX6/Screenshot.png)

[Klik her for at køre programmet](https://thundt.gitlab.io/aestetiskprgrammering/miniX6/index.html)

[Klik her for source koden](https://gitlab.com/Thundt/aestetiskprgrammering/-/blob/main/miniX6/miniX6.js)

## Mit program

I mit spil, Space Evaders, styrer man et rumfartøj, hvor det handler om at undvige de mod-flyvende asteroider. Rumfartøjet kan styres i 8 forskellige retninger, og jo længere tid man er i live, desto flere point får man. Spillet starter med en sort loading screen med den centraliserede tekst “Loading”. Efter et vis stykke tid popper der en “play” knap frem på skærmen, og når den klikkes, går spillet i gang. Spillet er inspireret af Tofu spillet, og viderebygget derfra, med et par ekstra features, og et anderledes mål. Man ser efter knappen 2 lag af stjerner der bevæger sig fra toppen af skærmen og ned, nogle større og hurtigere, og nogle mindre og langsommere. Dette skaber illusionen af bevægelse selvom man ikke bevæger sit fartøj. Hele formålet med loading screenen er faktisk at sørge for at stjernerne har dækket hele skærmen når spillet begynder, hvilket egentlig er essensen af abstraktion (også selvom det her kun dækker for manglende kode). Abstraktion handler overordnet om at samle og gemme mindre relevant data et sted hvor det ikke fylder.

### Objects

Asteroiderne, som drøner mod en, deler 1 til 1 de samme `constructor`s som stjernerne, hvilket inkluderer; en `this.speed`, `this.xPos/yPos`, move method og en show method. Hvor asteroiderne adskiller sig, er ved at deres afstand til spilleren bliver målt. Hvis spilleren kommer for tæt på en asteroide, kommer “play” knappen frem igen samt et “play again?” og en score counter, som under spillet har talt op en gang hvert sekund via `if(frameCount%60 == 0 && playerHasDied == false){score++}`. Som set her har jeg en variabel navngivet `playerHasDied`, denne variabel samt variablen `buttonHasBeenPressed` er i starten af programmet sat til false, og sammen er de med til at stoppe og starte spillet.

Mit mål med spillet har været at lave en så autentisk videogame følelse som muligt. Som jeg føler at jeg er kommet godt i mål med, dog ikke uden bugs (hvilket måske hjælper mit mål?). Start knappen kan altid klikkes, også midt i spillet, dette nulstiller ens score og sætter ens position tilbage til start, så det hjælper dog ikke spilleren. I modsætning til hvis spilleren finder ud af, at man kan styre ud for skærmen hvor asteroiderne ikke spawner, og hvor man derfor er i sikkerhed.

At kunne kode med objekter føler jeg har været et redskab jeg indtil nu har manglet. Man kan sige at objekter er en form for super variabel, der kan manipuleres på en meget mere avanceret måde. At kunne gemme en masse data, og derfra arbejde ud for at skabe et program, er i essens hvad objekt-kodning handler om. _“Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data” (Soon & Cox)_ 

## (Mit) Spil i en kulturel kontekst

Spil vil altid betyde noget forskelligt alt efter hvilken kulturel kontekst man placerer det i, og denne betydning kan variere meget. Et pragteksemplar på dette er hvordan man har set nogle spil blive bandlyst eller censureret i Kina, enten af deres voldelige natur eller af anden vest-præget oversag. Jeg føler ikke at mit spil hælder til en bestemt national kultur. Men for at forstå hvad man ser i spillet er det dog et krav, at man har en meget basal kendskab til illustration af rummet i popkultur. Jeg baserer hovedsageligt dette på at rumfartøjet ikke ligner dem vi har i vores tid.

Det giver rigtig god mening at spil benytter sig meget af object based coding, da der skal meget kode til at få et spil til at fungere. Jo mere data du kan gemme væk under objekter, jo bedre. Det sørger også for at koden bliver overskuelig at kigge på, og læse. Det giver også et godt billede af, hvorfor store spil tager så lang tid at lave, selv med objekt baseret kodning og abstraktion. Alt i alt et godt og vigtigt redskab.
