class SlowStar {
    constructor(){//contains the needed info
        this.speed = 1.5
        this.xPos = random(width)
        this.yPos = -5
    }
    move(){//moves the star
        this.yPos += this.speed
    }
    show(){//draws the star
        push()
        image(starImg,this.xPos,this.yPos,5,5)
        pop()
    }
}

class Star {
    constructor(){//contains the needed info
        this.speed = 2
        this.xPos = random(width)
        this.yPos = -5
    }
    move(){//draws the star
        this.yPos += this.speed
    }
    show(){//draws the star
        push()
        image(slowStarImg,this.xPos,this.yPos,7,7)
        pop()
    }
}