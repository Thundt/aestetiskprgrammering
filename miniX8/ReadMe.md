# miniX8

## Individuelt flowchart
Jeg valgte at lave et flowchart over min miniX6. Min miniX6 er et spil hvor man styre et rumskib som kan bevæge sig i 8 forskellige retninger. Spillet går ud på at undvige asteroider, som flyver imod en, og jo længere tid du overlever jo flere point for du.

![](https://gitlab.com/Thundt/aestetiskprgrammering/-/raw/main/miniX8/MyFlowchart.png)
### Fælles ReadMe findes [her](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/tree/main/MiniX_8)
