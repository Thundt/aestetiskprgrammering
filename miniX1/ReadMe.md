# miniX1 - Click a key
![](https://gitlab.com/Thundt/aestetiskprgrammering/-/raw/main/miniX1/MiniX1_Screenshot.png)

[Klik her for at køre programmet](https://thundt.gitlab.io/aestetiskprgrammering/miniX1/index.html)

[Klik her for source koden](https://thundt.gitlab.io/aestetiskprgrammering/miniX1/miniX1.js)

## Beskrivelse af programmet
Mit program er kort beskrevet en alternativ version af en digital 8-ball. Programmet beder dig ved opstart at trykke på en knap, når dette gøres vil noget tekst komme til syne, og hurtigt forsvinde igen. Teksten kommentere med tilfældige prompts på at du trykker på en knap.

Mit program bruger ironisk nok `draw()`funktionen til at "slette" hvad der kommer frem på skærmen ved at tegne en masse gennemsigtige cirkler uden omrids ved hjælp af `noStroke()`om og om igen tilfældige steder ved brug af `random()` funktionen. `random()`funktionen bliver også brugt til at trække tilfældige fraser fra string variablen "words" kreeret og navngivet vha. `let`. Dette bliver gjort hver gang en knap bliver trykket på ved hjælp af `keyPressed()`funktionen.
## Hvordan var det at kode?
Det er ikke helt første gang jeg giver mig til at prøve kode, men syntes stadig at det er svært at tyde logikken bag hvordan nogle ting virker. Det er meget slavisk med at kigge frem og tilbage på referencer, for så at prøve noget, og finde ud af at det ikke virker. Der viste sig en tydelig mangel på viden/skills i kodning, som ledte til en slags gentagne skuffende følelse, da jeg ikke kunne udføre ideerne som jeg fik. Dog var det en fed følelse da det endelig lykkedes, så på jagten af den følelse er jeg håbefuld på at den manglende viden/skill nok skal komme med tid. Forhåbentlig...

Originalt startede jeg et helt andet sted. Jeg ville animere nogle bølger, og have en delfin til at hoppe op ad vandet da der blev trykket på en knap. Dette viste sig at være rimelig optimistisk da jeg læste referencerne på nogle af de funktioner som jeg tænkte jeg skulle bruge, og endte derfor med at kombinere noget ud fra de funktioner.
## Hvordan er det anderledes end at skrive/læse normalt sprog
Hvis vi tager hensyn til at kodningen på en måde er en manual til computeren om hvad den skal gøre, og vi derved sammenligner med manualer (som jo bruger normalt sprog) så kommer struktureringen af sproget okay tæt på at ligne noget vi kunne bruge i normalt sprog. Der hvor kodning virkelig skiller sig ud, er ved tegnsætningen og de brugte ord. Hvis man f.eks. glemmer et punktum i en manual, så ville det nok gøre det sværere at forstå, men da vi mennesker er lidt bedre til at fortolke ting end en computer, går det nok alligevel, i modsætning til hvis man glemmer et semikolon i sin kode, så kan alt gå fra hinanden.
## Hvad betyder kode/kodning for mig?
Jeg benytter mig meget af moderne teknologier, og da de benytter sig meget af kode, kan jeg på den måde godt konkludere at kode betyder meget for mig.
Kodning er egentlig også rimelig vigtigt for min fremtidige karriere som lyddesigner, så jeg føler det er vigtigt for mig at lære. Om det så lige bliver p5.js/javascript der bliver sproget jeg en skønne dag hovedsageligt kommer til at kode i, tvivler jeg på, men jeg er sikker på at der ikke kan komme noget dårligt ud af kunne kode i det også.
