function setup()
{
  createCanvas(750, 500);
  background(255,255,255);
draw(); //Denne del sørger for at teksten "Press a Key" står på skærmen når programmet loader.
  {
  fill(0);
  textSize(100);
  text("Press a Key",115,275);
  }
}

function draw() //Denne del laver en masse gennemsigtige hvide cirkler som sørger for at slætte teksten som kommer til syne.
{ //noStroke sørger for at cirklerne ikke har noget omrids.
  noStroke();
  fill('rgba(255,255,255,0.25)');
  ellipse(random(750),random(500),300,300);
}

function keyPressed() //Denne del ser når der bliver trykket på en knap, og får derefter noget tekst til at komme til syne.
{
  //let skaber og navngiver en ny variabel, som består af en liste af forskellige fraser.
  let words = ["Damn!","Awesome!","You did it!","Very nice!","Good job!","Good pressin!","hmm","Well pressed!","decent","was that a key?","try again","I've seen better","that was weak","press lighter","There we go!","Good choice!","why that key?","poor choice"];
  //vi laver her en ny variabel, som står for a splitte fraserne ad, og give dem til os i tilfældig rækkefølge, når vi "kalder" på den.
  let word = random(words);
  //fill sørger for at teksten er sort.
  fill(0)
  textSize(65);
  //Dette er linjen som bestemmer hvor teksten kan komme frem. Uden denne linje kommer der ikke nogen tekst.
  text(word,random(25,300),random(50,475));
}