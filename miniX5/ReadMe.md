# miniX5 - Computer scribbles
![](https://gitlab.com/Thundt/aestetiskprgrammering/-/raw/main/miniX5/Screenshot.png)

[Klik her for at køre programmet](https://thundt.gitlab.io/aestetiskprgrammering/miniX5/index.html)

[Klik her for source koden](https://gitlab.com/Thundt/aestetiskprgrammering/-/blob/main/miniX5/miniX5.js)

## Mit program

Mit program går meget simpelt ud på, at tegne en streg, og derfra tegne en ny streg om og om igen indtil stregen rammer kanten af skærmen, og derfra begynder forfra fra midten af skærmen. Man har i programmet ikke nogen kontrol over hvilken vej stegen skal tegnes, men man har mulighed for at ændre på tykkelsen, og på længden af stregen. Programmet tegner 3 af disse streger pr. frame, og jeg kunne godt ha’ kodet en slider, som gav brugeren kontrol af dette, samt hastigheden af frameRaten, men jeg følte at det ville fylde for meget på skærmen med sliders. Ud over dette overvejede jeg en timer, hvis formål skulle være at måle, hvor langt tid stregen var om at finde skærmens rammer. Men af begrænset tid blev dette ikke til noget.

Jeg synes der sker noget meget interessant i mit program. Jeg fandt mig selv i næsten at forestille mig stregen som et form for entity, som prøvede at flygte fra skærmen. Skærmen blev i den her forstand en form for bur, hvilket man kan lave en meget dyb sammenligning til os som forbrugere. Men virkeligheden er egentlig bare at jeg ville kode en streg der fløj rundt på skærmen. _Line goes brrrrr_.

_For forklaring om hvordan mit program virker. Se “//” beskrivelserne i koden._

## Mine regler og mit programs emergente opførsel

Jeg vidste inden jeg startede på programmet, at der skulle være en vis tilfældighed indbygget, og fik meget hurtigt ideen om en streg der fortsat blev tegnet i en tilfældig retning. Dette blev min første regel, at lige meget hvad skulle stregen altid i en tilfældig retning. Jeg fandt under kodningen ud af, at stregen meget hurtigt ville have mulighed for at forsvinde væk fra skærmen, og var tæt på at lave det som en del af programmet, men tænkte at det var en fin mulighed for at introducerer min anden regel, “at stregen ikke måtte komme ud fra skærmen”.

Mit programs opførsel er ikke helt emergent, da brugeren har en form for kontrol over strengens længde. Dette påvirker også hvor hurtigt stregen bevæger sig over skærmen, og ultimativt hvor hurtigt den når kanten. Udover dette køre programmet helt selvstændigt, og kan på baggrund af programmets temporalitet køre i al evighed.

### Hvilken rolle spiller mine regler og processer

Min første regel er med til at gøre programmet interessant at kigge på i længere tid. Hvis programmet bare havde valgt en bestemt retning, og tegnet stregen i den retning, ville det meget hurtigt blive kedeligt. Min anden regel som jeg også lidt nævnte foroven, har med programmets temporalitet at gøre. Hvis linjen forsvandt fra skærmen, er der ikke noget at kigge på, og så bliver programmet igen kedeligt. Den anden regel sørger altså for at bevare hvad den første regel skaber.

## Ideen om auto-generatorer

Hvis man tager udgangspunkt i mit program, ville jeg ud fra citatet i bogen _print chaos_ våge at påstå, at det ville tælle som generativ kunst. _“Generative art refers to any art practice where [sic] artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art.”_ Dog er mit programs resultat ikke permanent, men jeg oplevede alligevel på tidspunkter under kodningen, at jeg kunne genkende bestemte former såsom et hjerte og en fugl. Selvfølgelig var det ikke kodet ind i programmet at den skulle danne disse former, men af ren kaos og tilfældighed blev de skabt. Mit program er kodet med chaos og tilfældighed i dets kerne, men selv denne tilfældighed har et system i computeren, og er i maskinen ikke tilfældigt. Det danner en illusion af tilfældighed som vi mennesker ikke kan opfatte, men som computeren godt kan, og på den måde snyder computeren os lidt.
