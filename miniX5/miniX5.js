let cosX //x kordinatet i det 1. punkt i linjen
let cosX1 //y kordinatet i det 1. punkt i linjen
let cosY //x kordinatet i det 2. punkt i linjen
let cosY1 //y kordinatet i det 2. punkt i linjen
let direction //Retningen som linjen bliver tegnet (fra 1-360 i grader)
let length //længden pr. linje
let lengthSlider
let thicknessSlider
let counter = 0

function setup() {
  createCanvas(windowWidth, windowHeight)
  background(20)
  angleMode(DEGREES)
  frameRate(30)
  //Her laver jeg 2 sliders som kommer til at kunne styre længden af stregen, og tykkelsen.
  lengthSlider = createSlider(1,30,1)
  lengthSlider.position(width/1.6, height/1.2)
  lengthSlider.style(`300px`)
  thicknessSlider = createSlider(1,20,1)
  thicknessSlider.position(width/3, height/1.2)
  thicknessSlider.style(`300px`)
  
  //Jeg giver her x og y kordinaterne til linjen et startpunkt.
  cosX = width/2
  cosY = height/2
  cosX1 = width/2
  cosY1 = height/2
}
function draw(){
  background(0,10)
  //Her tegnes de 2 sliders ind
  length = lengthSlider.value()
  thickness = thicknessSlider.value()
  fill(255)
  textSize(30)
  strokeWeight(0)
  text("Length",width/1.57, height/1.22)
  text("Thickness",width/3, height/1.22)
  //Indstillinger for selve linjen.
  strokeWeight(thickness)
  stroke(255)

  //Dette for-loop gør så programmet tegner 3 linjer af gangen
  for(let i=0;i<3;i++){
  //Syntaksen som tegner selve linjen.
  line(cosX,cosY,cosX1,cosY1)
    
  //Her bliver de første kordinator af linjen sat til at være de sidste kordinator. Dette sørger for at at linjen fortsætter fra hvor den sluttede.
  cosX=cosX1
  cosY=cosY1

  //Denne del står for at linjen ryger i en tilfældig retning, fra hvor linjen sidst sluttede.
  let direction  = random(1,360)
  cosX1 = cosX1 + length * cos(direction)
  cosY1 = cosY1 + length * sin(direction)

  //Denne sætter linjen tilbage til start, hvis den skulle komme uden for skærmen.
  if(cosX1>width || cosY1>height || cosX1<-0.1 || cosY1<-0.1){
    cosX1 = width/2
    cosY1 = height/2
  }
  }
}