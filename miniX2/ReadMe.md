# miniX2 - Overly Dramatic Smiley
![](https://gitlab.com/Thundt/aestetiskprgrammering/-/raw/main/miniX2/Screenshot.png)

[Klik her for at køre programmet](https://thundt.gitlab.io/aestetiskprgrammering/miniX2/index.html)

[Klik her for source koden](https://thundt.gitlab.io/aestetiskprgrammering/miniX2/miniX2.js)

 ## Beskrivelse af programmet

Jeg har lavet et program som starter ud med at vise en neutral simpel turkis emoji, en instruktion, og en lille advarsel om at programmet indeholder lyd. Når der klikkes på op-piletasten, ser man emojiens mund bevæge sig en smule opad, farven på emojien vil blive mere grøn, og man vil også høre noget idyllisk musik begynde at spille. Jo mere man klikker, og jo mere munden bevæger sig opad, jo højere bliver musikken, og jo kraftigere bliver den grønne farve. Når man klikker på ned-piletasten vil munden bevæges nedad, man vil se farven skifte mere mod blå, og man vil kunne høre en dyster musik spille i stedet.

### Hvad jeg har lært (Hvilke nye syntakser?)

Jeg har lært hvordan man tilføjer lyd til sit program, og hvordan man bruger "assets," altså filer. Udover dette føler jeg at jeg er blevet bedre til brugen af variabler, og har fået lidt bedre styr på logikken bag kodning. Jeg legede her meget rundt med syntaksen 'vertex', som tegner en streg med forskellige punkter, og i kombination med variabler, kan denne form transformeres på en mere avanceret måde. Jeg har også i dette program gjort brug af '&&', hvilket viste sig at være et meget effektivt redskab. Til sidst har jeg været inde og kigge på brug af `keyCode`, som er nødvendigt hvis vi vil have at bestemte taster skal kunne noget.
I lyddelen af programmet har jeg brugt funktionen `preload` som skal bruges når programmet gør brug af filer på computeren. Af syntakser har jeg brugt `soundFormats` som bestemmer hvilke filtyper programmet kan bruge, `loadSound` gør en fil klar til at blive brugt, `setVolume` som sætter lydstyrken af en bestemt lydfil, ved at referere til den variable du har givet lydfilen med "variableNavn.setVolume(0-1)". Ellers har jeg gjordt brug af syntakserne `.loop` som afspiller en fil om og om igen, `.stop` som stopper filen du referere til, og til sidst `.isPlaying` som udgiver en true eller false, alt efter om filen du refererer til er i gang med at bliver afspillet.

## Min emoji i en social og kulturel kontekst

Min emoji(s) har tilgangen at politik ikke behøver at være til stede i emojis, da de er et værktøj hovedsageligt til at udtrykke menneskelige følelser. Derfor er emojien meget simpel opstillet, med så lidt detaljer som muligt. Dette er en emoji, der ville kunne bruges i alle kulturer, da den ikke har en bestemt hudfarve eller unikke ansigtstræk. Derudover er der ikke vidst noget form for køn, så den ville kunne blive brugt ligeledes af mænd, kvinder og børn. Min emoji kommer ikke direkte ind på, hvorvidt politik i emojis ville kunne fungere, men sætter blot et eksempel for sig selv, og den type emoji som den er.
Variationen i emojien udforsker muligheden for variable emojis, så man selv lidt kan bestemme hvor glade, neutrale eller triste de skal være, og på denne måde give større udtrykkelsesmuligheder. Med mere avanceret kodning ville det også være muligt at frembringe andre og mere avanceret følelser.
Udover dette udforsker min emoji(s) også brugen af lyd til at udtrykke følelser, jeg mener ikke selv at som det er implementeret her, er noget som ville passe ind i vores samfund i dette givne tidspunkt, men jeg kunne godt forestille mig et samfund hvor emojis gav lyd fra sig, om det så er i form af musik eller lyd-effekter.
Jeg lavede som udgangspunkt min emoji til at være så upolitisk som mulig, hvilket jo i sig selv godt kan være en politisk drevet handling, drevet af neutralitet. Og man vil selvfølgelig altid kunne trække noget ind i en politisk kontekst, selv den mest politiske neutrale ting, og det har så været målet her at kreere en emoji som strider så langt væk fra det som muligt.
