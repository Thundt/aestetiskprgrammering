function preload(){
  //Vi "preloader" her de lydfiler som senere skal bruges i programmet, så de er klar til at blive afspillet med det samme.
  //Når vi skal bruge lyd-syntakser skal man også i sin HTML tilføje denne linje "<script src="../Libraries/p5.sound.js"></script>"
  soundFormats(`mp3`)
  STBad = loadSound(`./assets/bad.mp3`)
  STGood = loadSound(`./assets/good.mp3`)
}

//Her defineres de to variabler som står for lydstyrken. (De er nul, da de stille og roligt skal blive højere)
let BVol = 0
let GVol = 0

//Variabler for de forskellige punkter i mundens Y-kordinator
let mouthCenter = 220
let mouthCenterMid = 220
let mouthMid = 220
let mouthMidEdge = 220
let mouthEdge = 220
//Variabler for farven på emoji'en
let g = 255
let b = 255

function draw()
{
createCanvas(400, 400)
background(220,220,220)
//Dette er funktionen som faktisk sætter lydstyrken.
STBad.setVolume(BVol)
STGood.setVolume(GVol)
//Hoved
strokeWeight(5)
fill(50,g,b)
ellipse(200,200,125)
//Mund
fill(0)
strokeWeight(8)
strokeJoin(ROUND)
smooth()
beginShape()
vertex(170, mouthEdge)
vertex(175, mouthMidEdge)
vertex(180, mouthMid)
vertex(190, mouthCenterMid)
vertex(200, mouthCenter)
vertex(210, mouthCenterMid)
vertex(220, mouthMid)
vertex(225, mouthMidEdge)
vertex(230, mouthEdge)
vertex(170, mouthEdge)
endShape();
//Øjne
strokeWeight(10)
fill(0)
ellipse (180,185,10)
ellipse (220,185,10)
//Tekst
textSize(25)
text("Click up-arrow or down-arrow!", 35, 80)
text("Sound Warning!", 110, 340)

//Dette kode sørger for at de to musik-stykker spiller når de skal
if (mouthEdge < 220 && STGood.isPlaying()==false){
  STGood.loop()
}
if (mouthEdge >= 220 && STGood.isPlaying()==true){
  STGood.stop()
}
if (mouthEdge > 220 && STBad.isPlaying()==false){
  STBad.loop()
}
if (mouthEdge <= 220 && STBad.isPlaying()==true){
  STBad.stop()
}
}

//Her siger vi at hvis der bliver trykket på op-piltasten at munden skal bevæge sig opad, lydstyrken skal blive højere, og farven skal ændres.
function keyPressed(){
  if (keyCode === UP_ARROW && mouthEdge > 210){
  //Mund-possition
  mouthCenter = mouthCenter + 0.5
  mouthCenterMid = mouthCenterMid + 0.4
  mouthMid = mouthMid + 0.2
  mouthMidEdge = mouthMidEdge 
  mouthEdge = mouthEdge - 0.5
  //Lydstyrke
  BVol = BVol - 0.05
  GVol = GVol + 0.05
  STGood.setVolume(GVol)
  //Farve
  b = b - 12
  g = g + 12
}
//Hvis det istedet er ned-pilen, skal det modsatte ske.
else if (keyCode === DOWN_ARROW && mouthEdge < 230){
  //Mund-possition
  mouthCenter = mouthCenter - 0.5
  mouthCenterMid = mouthCenterMid - 0.4
  mouthMid = mouthMid - 0.2
  mouthMidEdge = mouthMidEdge 
  mouthEdge = mouthEdge + 0.5
  //Lydstyrke
  GVol = GVol - 0.05
  BVol = BVol + 0.05
  STBad.setVolume(BVol)
  //Farve
  g = g - 12
  b = b + 12
}}

